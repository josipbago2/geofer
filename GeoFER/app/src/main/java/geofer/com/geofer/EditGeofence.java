package geofer.com.geofer;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

import geofer.com.geofer.pojo.Fence;
import geofer.com.geofer.utilities.Const;
import geofer.com.geofer.utilities.GeofenceTransitionsIntentService;
import geofer.com.geofer.utilities.Util;

/**
 * Created by Josip on 8.5.2016..
 */
public class EditGeofence extends AppCompatActivity
        implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, ResultCallback<Status> {

    private static final int REQUEST_CODE = 500;

    private GoogleApiClient mGoogleApiClient;

    private Button btnEditFence, setLocation;
    private EditText etTitle, etDesc;
    private TextView tvLat, tvLng, tvRadius;

    private CheckBox cbEnter, cbExit;
    private boolean enter = true, exit = false;

    private ArrayList<Fence> arrayListFences;
    private Fence fenceToEdit;

    private int radius;
    private double lat, lng;
    private String title, desc, id;

    // Intent data: id, mylat, mylng

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_creategeofence);

        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvLat = (TextView) findViewById(R.id.tv_lat);
        tvLng = (TextView) findViewById(R.id.tv_lng);
        tvRadius = (TextView) findViewById(R.id.tv_radius);
        setLocation = (Button) findViewById(R.id.btn_chooselocation);
        etTitle = (EditText) findViewById(R.id.et_title);
        etDesc = (EditText) findViewById(R.id.et_desc);
        btnEditFence = (Button) findViewById(R.id.btn_createfence);

        cbEnter = (CheckBox) findViewById(R.id.cbEnter);
        cbExit = (CheckBox) findViewById(R.id.cbExit);

        cbEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (enter) {
                    cbEnter.setChecked(false);
                    enter = false;
                } else {
                    cbEnter.setChecked(true);
                    enter = true;
                }
            }
        });

        cbExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (exit) {
                    cbExit.setChecked(false);
                } else {
                    cbExit.setChecked(true);
                }
            }
        });

        arrayListFences = new ArrayList<>();
        arrayListFences.addAll(Util.loadFenceList(this));
        findFenceToEditById();

        setLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditGeofence.this, SetLocationFence.class);
                intent.putExtra(Const.LONGITUDE_EXTRA, getIntent().getExtras().getDouble(Const.LONGITUDE_EXTRA));
                intent.putExtra(Const.LATITUDE_EXTRA, getIntent().getExtras().getDouble(Const.LATITUDE_EXTRA));
                intent.putExtra(Const.RADIUS_EXTRA, getIntent().getExtras().getInt(Const.RADIUS_EXTRA));
                startActivityForResult(intent, REQUEST_CODE);
            }
        });

        btnEditFence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etTitle.getText().toString().isEmpty()) {
                    if (cbEnter.isChecked() || cbExit.isChecked()) {
                        arrayListFences.remove(fenceToEdit);
                        removeOldFence(fenceToEdit);

                        fenceToEdit = new Fence();
                        fenceToEdit.setLng(lng);
                        fenceToEdit.setLat(lat);
                        fenceToEdit.setName(etTitle.getText().toString());
                        fenceToEdit.setRadius(radius);
                        fenceToEdit.setDesc(etDesc.getText().toString());
                        fenceToEdit.setId(id);

                        arrayListFences.add(fenceToEdit);
                        Util.saveFenceList(getBaseContext(), arrayListFences);
                        addNewFence(fenceToEdit);

                        finish();
                    } else {
                        Toast.makeText(getBaseContext(), "Please select type of notification (enter/exit)", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getBaseContext(), "Please enter title", Toast.LENGTH_SHORT).show();
                }

            }
        });

        btnEditFence.setText("Edit Geofence");

        buildGoogleApiClient();
    }

    private void removeOldFence(Fence fence) {
        List<String> list = new ArrayList<>();
        list.add(fence.getId());
        LocationServices.GeofencingApi.removeGeofences(mGoogleApiClient, list);
    }

    private void addNewFence(Fence fence) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.GeofencingApi.addGeofences(mGoogleApiClient, createGeofence(fence.getId()), createPendingIntent()).setResultCallback(this);
    }

    private PendingIntent createPendingIntent() {
        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
        // addGeofences() and removeGeofences().
        return PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private GeofencingRequest createGeofence(String hashId) {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        if (cbEnter.isChecked() && cbExit.isChecked()) {
            builder.addGeofence(new Geofence.Builder()
                    .setRequestId(hashId)
                    .setCircularRegion(lat, lng, radius)
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT).build());
        } else if (cbEnter.isChecked() && !cbExit.isChecked()) {
            builder.addGeofence(new Geofence.Builder()
                    .setRequestId(hashId)
                    .setCircularRegion(lat, lng, radius)
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER).build());
        } else if (!cbEnter.isChecked() && cbExit.isChecked()) {
            builder.addGeofence(new Geofence.Builder()
                    .setRequestId(hashId)
                    .setCircularRegion(lat, lng, radius)
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_EXIT).build());
        } else {
            builder.addGeofence(new Geofence.Builder()
                    .setRequestId(hashId)
                    .setCircularRegion(lat, lng, radius)
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT).build());
        }
        return builder.build();
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void findFenceToEditById() {
        for (Fence i : arrayListFences) {
            if (i.getId().equals(getIntent().getExtras().getString(Const.FENCE_ID_EXTRA))) {

                radius = i.getRadius();
                lat = i.getLat();
                lng = i.getLng();
                title = i.getName();
                desc = i.getDesc();
                id = i.getId();

                fenceToEdit = new Fence();
                fenceToEdit = i;

                etTitle.setText(i.getName());
                etDesc.setText(i.getDesc());
                tvRadius.setText("Radius: " + String.valueOf(i.getRadius()) + "m");
                tvLat.setText("Lat: " + String.valueOf(i.getLat()));
                tvLng.setText("Lng: " + String.valueOf(i.getLng()));
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE) {
                radius = data.getExtras().getInt(Const.RADIUS_EXTRA);
                lat = data.getExtras().getDouble(Const.LATITUDE_EXTRA);
                lng = data.getExtras().getDouble(Const.LONGITUDE_EXTRA);
                tvRadius.setText("Radius: " + String.valueOf(radius) + "m");
                tvLat.setText("Lat: " + String.valueOf(lat));
                tvLng.setText("Lng: " + String.valueOf(lng));
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull Status status) {

    }
}
