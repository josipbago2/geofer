package geofer.com.geofer;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

import geofer.com.geofer.adapters.ListAdapterFences;
import geofer.com.geofer.pojo.Fence;
import geofer.com.geofer.utilities.SharedPrefencesHandler;
import geofer.com.geofer.utilities.Util;

/**
 * Created by Josip on 8.5.2016..
 */
public class MyFences extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<Status> {

    private ListView listView;
    private ArrayList<Fence> arrayList;
    private ListAdapterFences listAdapter;

    private GoogleApiClient mGoogleApiClient;

    private TextView tvMessage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_listview);

        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = (ListView) findViewById(R.id.list);
        tvMessage = (TextView) findViewById(R.id.tv_message);

        buildGoogleApiClient();
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
        getFences();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    private void getFences() {
        arrayList = new ArrayList<>();
        arrayList.addAll(Util.loadFenceList(this));
        setList();
    }

    public void removeFenceDueToDelete(Fence fence) {
        removeGeofence(fence.getId());
        arrayList.remove(fence);
        setList();
    }

    public void removeGeofence(String hashId) {
        List<String> list = new ArrayList<>();
        list.add(hashId);
        LocationServices.GeofencingApi.removeGeofences(mGoogleApiClient, list);
    }

    private void setList() {
        if (arrayList.size() == 0) {
            tvMessage.setText("No Geofences created");
            tvMessage.setVisibility(View.VISIBLE);
        } else {
            tvMessage.setVisibility(View.INVISIBLE);
        }
        listAdapter = new ListAdapterFences(this, arrayList);
        listView.setAdapter(listAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Util.saveFenceList(this, arrayList);
        SharedPrefencesHandler.setResumeBlocker(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull Status status) {

    }
}
