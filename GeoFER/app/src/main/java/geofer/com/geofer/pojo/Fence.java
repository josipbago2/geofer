package geofer.com.geofer.pojo;

/**
 * Created by Josip on 8.5.2016..
 */
public class Fence {

    String id;
    String name, desc;
    double lat, lng;
    int radius;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    };

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }
}
