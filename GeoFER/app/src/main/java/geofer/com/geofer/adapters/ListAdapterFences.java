package geofer.com.geofer.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;

import geofer.com.geofer.EditGeofence;
import geofer.com.geofer.MyFences;
import geofer.com.geofer.R;
import geofer.com.geofer.pojo.Fence;
import geofer.com.geofer.utilities.Const;
import geofer.com.geofer.utilities.Util;

/**
 * Created by Josip on 8.5.2016..
 */
public class ListAdapterFences extends BaseAdapter {
    private Context context;
    private ArrayList<Fence> listData;

    public ListAdapterFences(Context context, ArrayList<Fence> listData) {
        this.listData = listData;
        this.context = context;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_fence, null);
            viewHolder.tvDesc = (TextView) convertView.findViewById(R.id.tv_desc);
            viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
            viewHolder.btnEdit = (Button) convertView.findViewById(R.id.btn_edit);
            viewHolder.btnDelete = (Button) convertView.findViewById(R.id.btn_delete);
            viewHolder.btnShowOnMap = (Button) convertView.findViewById(R.id.btn_showonmap);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvDesc.setText(listData.get(position).getDesc());
        viewHolder.tvTitle.setText(listData.get(position).getName());

        viewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EditGeofence.class);
                intent.putExtra(Const.FENCE_ID_EXTRA, listData.get(position).getId());
                intent.putExtra(Const.LONGITUDE_EXTRA, listData.get(position).getLng());
                intent.putExtra(Const.LATITUDE_EXTRA, listData.get(position).getLat());
                intent.putExtra(Const.RADIUS_EXTRA, listData.get(position).getRadius());
                context.startActivity(intent);

            }
        });

        viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                alertBuilder.setTitle("Complete request");
                alertBuilder.setMessage("Are you sure you want to delete geofence: " + listData.get(position).getName() + "?");
                alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertBuilder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ArrayList<Fence> myFences = new ArrayList<>();
                        myFences.addAll(Util.loadFenceList(context));
                        myFences.remove(listData.get(position));
                        Util.saveFenceList(context, myFences);
                        ((MyFences) context).removeFenceDueToDelete(listData.get(position));
                    }
                });
                AlertDialog alertDialog = alertBuilder.create();
                alertDialog.show();
            }


        });

        viewHolder.btnShowOnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent result = new Intent();
                result.putExtra(Const.LONGITUDE_EXTRA, listData.get(position).getLng());
                result.putExtra(Const.LATITUDE_EXTRA, listData.get(position).getLat());
                ((MyFences) context).setResult(Activity.RESULT_OK, result);
                ((MyFences) context).finish();
            }
        });

        return convertView;
    }

    class ViewHolder {
        TextView tvTitle, tvDesc;
        Button btnEdit, btnShowOnMap, btnDelete;
    }
}
