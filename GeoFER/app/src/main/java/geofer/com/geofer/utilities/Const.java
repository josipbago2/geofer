package geofer.com.geofer.utilities;

/**
 * Created by Josip on 9.5.2016..
 */
public class Const {
    public static final String LATITUDE_EXTRA = "lat";
    public static final String LONGITUDE_EXTRA = "long";
    public static final String RADIUS_EXTRA = "radius";

    public static final String FENCE_ID_EXTRA = "id";
}
