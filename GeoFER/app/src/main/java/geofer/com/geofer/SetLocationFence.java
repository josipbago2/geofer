package geofer.com.geofer;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import geofer.com.geofer.utilities.Const;

/**
 * Created by Josip on 15.5.2016..
 */
public class SetLocationFence extends AppCompatActivity implements OnMapReadyCallback {

    // intent mylat, mylng
    private GoogleMap mMap;
    private UiSettings uiSettings;

    private double myLat, myLng;

    private int radius = 0;

    private SeekBar radiusControl;
    private TextView tvRadius;
    private Marker markerFenceCenter;
    private Circle circleFence;

    private static final int stepSize = 20;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_setlocation);

        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        myLat = getIntent().getExtras().getDouble(Const.LATITUDE_EXTRA);
        myLng = getIntent().getExtras().getDouble(Const.LONGITUDE_EXTRA);
        radius = getIntent().getIntExtra(Const.RADIUS_EXTRA, 100);

        Toast.makeText(this, "Long click on the marker to drag it around the map, so you can customize your fence position", Toast.LENGTH_LONG).show();

        radiusControl = (SeekBar) findViewById(R.id.seekBar);
        tvRadius = (TextView) findViewById(R.id.tv_radius);

        tvRadius.setText(String.valueOf(radius));
        radiusControl.setProgress(radius);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        radiusControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress = ((int) Math.round(progress / stepSize)) * stepSize;
                seekBar.setProgress(progress);
                tvRadius.setText("Radius: " + progress + "m");
                radius = progress;
                updateCircleRadius(radius);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        uiSettings = mMap.getUiSettings();
        uiSettings.setZoomControlsEnabled(true);
        uiSettings.setZoomGesturesEnabled(true);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        }
        updateMapWithLocationOfFence();

    }


    private void updateCircleRadius(int radius) {
        circleFence.setRadius(radius);
    }

    private void updateMapWithLocationOfFence() {
        LatLng myLocation = new LatLng(myLat, myLng);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLng(myLocation));
        mMap.moveCamera(CameraUpdateFactory.zoomTo(15));
        markerFenceCenter = mMap.addMarker(new MarkerOptions().position(myLocation).draggable(true));

        circleFence = mMap.addCircle(new CircleOptions()
                .center(myLocation)
                .radius(radius).strokeColor(Color.RED).strokeWidth(2).fillColor(Color.parseColor("#99F78181")));

        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {
                circleFence.setCenter(marker.getPosition());
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_two, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent resultIntent = new Intent();
        resultIntent.putExtra(Const.LATITUDE_EXTRA, markerFenceCenter.getPosition().latitude);
        resultIntent.putExtra(Const.LONGITUDE_EXTRA, markerFenceCenter.getPosition().longitude);
        resultIntent.putExtra(Const.RADIUS_EXTRA, radius);
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent resultIntent;
        switch (item.getItemId()) {
            case R.id.done:
                resultIntent = new Intent();
                resultIntent.putExtra(Const.LATITUDE_EXTRA, markerFenceCenter.getPosition().latitude);
                resultIntent.putExtra(Const.LONGITUDE_EXTRA, markerFenceCenter.getPosition().longitude);
                resultIntent.putExtra(Const.RADIUS_EXTRA, radius);
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
                break;
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
