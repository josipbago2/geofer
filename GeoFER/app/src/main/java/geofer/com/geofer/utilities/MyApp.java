package geofer.com.geofer.utilities;

import android.app.Application;

import geofer.com.geofer.utilities.SharedPrefencesHandler;

/**
 * Created by Josip on 8.5.2016..
 */
public class MyApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        initialize();
    }

    protected void initialize() {
        SharedPrefencesHandler.initSharedPreferencesHandler(getApplicationContext());
    }
}
