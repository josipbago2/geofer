package geofer.com.geofer.utilities;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Josip on 10.5.2016..
 */
public class SharedPrefencesHandler {
    static SharedPreferences prefs;
    static SharedPreferences.Editor prefsEditor;

    public static void initSharedPreferencesHandler(Context context) {
        prefs = context.getSharedPreferences("prefs", Context.MODE_PRIVATE);
        prefsEditor = prefs.edit();
    }

    public static boolean isResumeBlocked() {
        return prefs.getBoolean("resumeblocked", false);
    }

    public static void setResumeBlocker(boolean block) {
        prefsEditor.putBoolean("resumeblocked", block);
        prefsEditor.apply();
    }

}
