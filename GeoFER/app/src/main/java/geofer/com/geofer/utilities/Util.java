package geofer.com.geofer.utilities;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Random;

import geofer.com.geofer.pojo.Fence;

/**
 * Created by Josip on 9.5.2016..
 */
public class Util {

    public static String generateHashId() {
        Random rnd = new Random();
        return String.valueOf(rnd.nextDouble());
    }

    public static synchronized void saveFenceList(Context context, ArrayList<Fence> callLog) {
        SharedPreferences mPrefs = context.getSharedPreferences("array", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(callLog);
        prefsEditor.putString("fences", json);
        prefsEditor.apply();
    }

    public static ArrayList<Fence> loadFenceList(Context context) {
        ArrayList<Fence> arrayList = new ArrayList<>();
        SharedPreferences mPrefs = context.getSharedPreferences("array", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("fences", "");
        if (!json.isEmpty()) {
            Type type = new TypeToken<ArrayList<Fence>>() {
            }.getType();
            arrayList = gson.fromJson(json, type);
        }
        return arrayList;
    }

}
